<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alunos extends Model
{
    protected $fillable = [
        'nome',
        'email',
        'turma_id',
        'imagem',
        'status'
    ];
}
