<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class clientes extends Model
{
    //protege os campos
    protected $fillable = [
        'nome',
        'endereco',
        'numero'
    ];


}
