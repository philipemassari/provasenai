<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Alunos;

use Redirect;
use Storage;

class AlunosController extends Controller
{
    public function index(){
        $alunos = Alunos::get();
        return view('alunos.lista', ['alunos' => $alunos]);
    }

    public function novo(){
        return view('alunos.formulario');
    }

    public function salvar(Request $request){
    $alunos = new Alunos();
    // $alunos = $alunos->create([$request->all()]);
    // $base64_image = base64_encode(file_get_contents($request->input('imagem')));
    
    $base64_image = 'teste.png'; // your base64 encoded     
    // @list($type, $file_data) = explode(';', $base64_image);
    // @list(, $file_data) = explode(',', $file_data); 
    // $imageName = str_random(10).'.'.'png';   
    // Storage::disk('img')->put($imageName, base64_decode($file_data));
    // Storage::put('file.jpg', $encoded_image);

    
    $alunos = $alunos->create(['nome' => $request->nome, 'email' => $request->email, 'turma_id' => $request->turma_id, 'imagem' => $base64_image, 'status' => '1']);
    \Session::flash('mensagem_sucesso','Aluno cadastrado com sucesso!');
    return Redirect::to('alunos');
    }

    public function editar($id){
       $aluno = Alunos::findOrFail($id);
       return view('alunos.formulario', ['aluno' => $aluno]);
    }
    public function atualizar($id, Request $request){
        $aluno = Alunos::findOrFail($id);
        $aluno->update(['nome' => $request->nome, 'email' => $request->email, 'turma_id' => $request->turma_id, 'imagem' => 'teste.png', 'status' => '1']);
        \Session::flash('mensagem_sucesso','Aluno atualizado com sucesso!');
        return Redirect::to('alunos');
     }

     public function deletar($id, Request $request){
        $aluno = Alunos::findOrFail($id);
        $aluno->delete();
        \Session::flash('mensagem_sucesso','Aluno deletado com sucesso!');
        return Redirect::to('alunos');
     }

     public function visualizar($id, Request $request){
        $aluno = Alunos::findOrFail($id);
        return view('alunos.formulario', ['aluno' => $aluno]);
     }


}
