<?php

use Illuminate\Database\Seeder;
use App\Turmas;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // $this->call(TurmasSeeder::class);

        Turmas::create([

            'Turma' => 'Turma - 1'
        ]);

        Turmas::create([

            'Turma' => 'Turma - 2'
        ]);

        Turmas::create([

            'Turma' => 'Turma - 3'
        ]);

        Turmas::create([

            'Turma' => 'Turma - 4'
        ]);

        Turmas::create([

            'Turma' => 'Turma - 5'
        ]);
    }
}
