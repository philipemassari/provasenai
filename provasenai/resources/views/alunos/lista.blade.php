@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Alunos</div>
                
                
                <div class="card-body">
                <a class="pull-right btn btn-primary" href="{{ url('alunos/novo') }}">Novo Aluno</a>
                <br/> <br/>
                    <!-- @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif -->
                    @if(Session::has('mensagem_sucesso'))
                    <div class="alert alert-success">{{ Session::get('mensagem_sucesso') }}</div>
                    @endif
                    <strong>Listagem de Alunos</strong>
                     <br/>
                     <br/>
                     <div class="container">
    
  <table class="table">
    <thead>
      <tr>
        <th>Nome</th>
        <th>Email</th>
        <th>Turma</th>
        <th>Menu</th>
      </tr>
    </thead>
    <tbody>
    @foreach($alunos as $aluno)
      <tr>
        <td>{{ $aluno->nome }}</td>
        <td>{{ $aluno->email }}</td>
        <td>{{ $aluno->turma_id }}</td>
        <td>
        <a href="alunos/{{ $aluno->id }}/editar" class="btn btn-sm btn btn-primary">Editar</a>
       
        {!! Form::open(['method' => 'DELETE', 'url' => 'alunos/'.$aluno->id, 'style' => 'display: inline;']) !!}
        <button type="submit" class="btn btn-sm">Excluir</button>
        {!! Form::close() !!}
        <a href="alunos/{{ $aluno->id }}/visualizar" class="btn btn-sm btn-outline-dark">Visualizar</a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>



                </div>
            </div>
        </div>
    </div>
</div>
@endsection
