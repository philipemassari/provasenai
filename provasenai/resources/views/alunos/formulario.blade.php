@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Alunos</div>

                <div class="card-body">
                    <!-- @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif -->

                    @if(Request::is('*/visualizar'))
                      <strong>Nome:</strong> {{ $aluno->nome }}
                      <br/>
                      <strong>Email:</strong> {{ $aluno->email }}
                      <br/>
                      <strong>Turma:</strong> {{ $aluno->turma_id }}
                    @endif    


                    @if(Request::is('*/editar'))
                        {!! Form::model($aluno, ['method' => 'PATCH', 'url' => 'alunos/'.$aluno->id]) !!}
                        {!! Form::label('nome', "Nome") !!}
                        {!! Form::input('text', 'nome', null, ['class' => 'form-control', 'autofocus', 'placeholder' => 'Nome', 'required']) !!}
                        <br/>
                        {!! Form::label('email', "Email") !!}
                        {!! Form::input('text', 'email', null, ['class' => 'form-control', 'autofocus', 'placeholder' => 'Email', 'required']) !!}
                        <br/>
                        {!! Form::label('turma', "Turma") !!}
                        {!! Form::select('turma_id', array('1' => 'Turma 1', '2' => 'Turma 2', '3' => 'Turma 3', '4' => 'Turma 4', '5' => 'Turma 5', 'required')) !!}
                        
                        &ensp;&ensp;

                        {!! Form::label('imagem', "Imagem") !!}
                        {!! Form::file('imagem') !!}
                        <!-- {!! Form::input('text', 'imagem', null, ['class' => 'form-control', 'autofocus', 'placeholder' => 'Imagem']) !!} -->
                        <br/><br/>
                    {!! Form::submit('salvar', ['class'=>'btn btn-primary']) !!}
                    
                    
                    {!! Form::close() !!}
                    @endif    
                    
                    @if(Request::is('*/novo'))
                        {!! Form::open(['url' => 'alunos/salvar']) !!}
                        {!! Form::label('nome', "Nome") !!}
                        {!! Form::input('text', 'nome', null, ['class' => 'form-control', 'autofocus', 'placeholder' => 'Nome','required']) !!}
                        <br/>
                        {!! Form::label('email', "Email") !!}
                        {!! Form::input('text', 'email', null, ['class' => 'form-control', 'autofocus', 'placeholder' => 'Email','required']) !!}
                        <br/>
                        {!! Form::label('turma', "Turma") !!}
                        {!! Form::select('turma_id', array('1' => 'Turma 1', '2' => 'Turma 2', '3' => 'Turma 3', '4' => 'Turma 4', '5' => 'Turma 5','required')) !!}
                        
                        &ensp;&ensp;

                        {!! Form::label('imagem', "Imagem") !!}
                        {!! Form::file('imagem') !!}
                        <!-- {!! Form::input('text', 'imagem', null, ['class' => 'form-control', 'autofocus', 'placeholder' => 'Imagem']) !!} -->
                        <br/><br/>
                    {!! Form::submit('salvar', ['class'=>'btn btn-primary']) !!}
                    
                    
                    {!! Form::close() !!}
                    @endif    

                    
                       


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
