<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


// Route::get('/', 'PagesController@index');

Route::group(['middleware' => 'web'], function (){
    Route::get('/', 'HomeController@index');
    Auth::routes();
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('alunos', 'AlunosController@index');
    Route::get('alunos/novo', 'AlunosController@novo');
    Route::get('alunos/{aluno}/editar', 'AlunosController@editar');
    Route::post('alunos/salvar', 'AlunosController@salvar');
    Route::patch('alunos/{aluno}', 'AlunosController@atualizar');
    Route::delete('alunos/{aluno}', 'AlunosController@deletar');
    Route::get('alunos/{aluno}/visualizar', 'AlunosController@visualizar');

});



// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
